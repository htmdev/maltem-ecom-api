package org.maltem.ecom;

import net.bytebuddy.utility.RandomString;
import org.maltem.ecom.consumers.CategoryRepository;
import org.maltem.ecom.consumers.ProductRepository;
import org.maltem.ecom.entities.Category;
import org.maltem.ecom.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import java.util.Random;

@SpringBootApplication
public class EcomApiApplication implements CommandLineRunner {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    RepositoryRestConfiguration repositoryRestConfiguration;
    public static void main(String[] args) {
        SpringApplication.run(EcomApiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        repositoryRestConfiguration.exposeIdsFor(Product.class);
        repositoryRestConfiguration.exposeIdsFor(Category.class);
        Category clothes = categoryRepository.save(new Category("Clothes",null,null));
        Product p1 = new Product();
        p1.setName("Green shirt");
        p1.setAvailable(true);
        p1.setDescription("Green shirt high quality");
        p1.setRating(3);
        p1.setStockAvailability(10);
        p1.setPicture("product_2.jpg");
        p1.setCategory(clothes);
        productRepository.save(p1);
        Product p2 = new Product();
        p2.setName("Sweat-shirt");
        p2.setAvailable(true);
        p2.setDescription("Sweat-shirt high quality");
        p2.setRating(3);
        p2.setPicture("product_3.jpg");
        p2.setStockAvailability(10);
        p2.setCategory(clothes);
        productRepository.save(p2);
        Category electronics = categoryRepository.save(new Category("Electronics",null,null));
        Product p3 = new Product();
        p3.setName("Smart watch");
        p3.setAvailable(true);
        p3.setDescription("Smart watch high quality");
        p3.setRating(3);
        p3.setPicture("product_5.jpg");
        p3.setStockAvailability(10);
        p3.setCategory(electronics);
        productRepository.save(p3);
        categoryRepository.save(new Category("Healthcare",null,null));
        categoryRepository.save(new Category("Fitness",null,null));
    }
}
