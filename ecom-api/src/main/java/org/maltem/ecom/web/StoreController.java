package org.maltem.ecom.web;

import org.maltem.ecom.consumers.ProductRepository;
import org.maltem.ecom.entities.Product;
import org.maltem.ecom.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
public class StoreController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping(path ="/getProductPicture/{id}",produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] getProductPicture(@PathVariable("id") Long id) throws IOException {
        Product p = productRepository.getById(id);
       return Files.readAllBytes(Paths.get(System.getProperty("user.home")+ Constantes.PICTURES_PATH +p.getPicture()));
    }
}
