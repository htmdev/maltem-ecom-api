package org.maltem.ecom.entities;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Category extends BaseEntity implements Serializable {

    private String name;
    private String description;
    @OneToMany(mappedBy = "category")
    private Collection<Product> products;

    public Category(String name, String description, Collection<Product> products) {
        this.name = name;
        this.description = description;
        this.products = products;
    }

    public Category() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
