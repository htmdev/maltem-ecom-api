package org.maltem.ecom.entities;


import org.springframework.data.annotation.CreatedBy;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Id
    @SequenceGenerator(name="all_entites",sequenceName = "all_entites",allocationSize = 1)
    @GeneratedValue(strategy =SEQUENCE,generator = "all_entites")
    protected Long id;

    @XmlTransient
    @Column(length = 20)
    protected String createdByUser;

    @XmlTransient
    @Column(length = 20)
    @CreatedBy
    protected String updatedByUser;

    @XmlTransient
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdOn;

    @XmlTransient
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updatedOn;

    @Version
    @Column(name = "VERSION")
    protected Long version;

    public BaseEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getUpdatedByUser() {
        return updatedByUser;
    }

    public void setUpdatedByUser(String updatedByUser) {
        this.updatedByUser = updatedByUser;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
