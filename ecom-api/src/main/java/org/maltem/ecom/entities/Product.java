package org.maltem.ecom.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class Product extends BaseEntity implements Serializable {

    private String name;
    private String description;
    private BigDecimal price;
    private int rating;
    private int stockAvailability;
    private boolean available;
    private String picture;
    @ManyToOne
    private Category category;

    public Product(String name, String description, BigDecimal price, int rating, int stockAvailability, boolean available, String picture, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.rating = rating;
        this.stockAvailability = stockAvailability;
        this.available = available;
        this.picture = picture;
        this.category = category;
    }

    public Product() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getStockAvailability() {
        return stockAvailability;
    }

    public void setStockAvailability(int stockAvailability) {
        this.stockAvailability = stockAvailability;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
