import {Component, OnInit} from '@angular/core';
import {ProductsService} from "./service/products.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  title = 'maltem-ecom-front';
  categories : any;
  constructor(private service : ProductsService) {
  }
  ngOnInit(): void {
    this.getAllCategories();
  }

  public getAllCategories(){
    this.service.getAllCategories().subscribe( res =>{
      this.categories = res
    },
      err =>{
      console.log(err)
      })
  }
}
