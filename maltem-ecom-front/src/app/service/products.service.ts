import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http:HttpService) { }

  public getAllCategories(){
    return this.http.get("categories")
  }
  public getAllProducts(){
    return this.http.get("products")
  }
  public getProductsCategory(idCateg : string){
    return this.http.get("categories/"+idCateg+"/products")
  }
}
