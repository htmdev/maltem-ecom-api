import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../../service/products.service";
import {ActivatedRoute} from "@angular/router";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
   products : any;
   idCateg : any;
   env = environment;
  constructor(private productService: ProductsService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params =>{
      this.idCateg = params.get('id');
      if(this.idCateg){
        this.getCategoryProducts(this.idCateg)
      }else {
        this.getProductsList()
      }
    } );

  }

  getProductsList(){
    this.productService.getAllProducts().subscribe(
      res =>{
        this.products = res
      },err => {
        console.log(err)
      }
    )
  }
  getCategoryProducts(id : string){
    this.productService.getProductsCategory(id).subscribe(res =>{
      this.products = res
    },err => {
      console.log(err)
    })
  }
}
